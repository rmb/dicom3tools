static const char *CopyrightIdentifier(void) { return "@(#)himrdmpf.cc Copyright (c) 1993-2022, David A. Clunie DBA PixelMed Publishing. All rights reserved."; }
#include "himrdmp.h"
#include "himrptrs.h"
#include "himrdmpf.h"
